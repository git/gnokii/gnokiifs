Name: gnokiifs
Summary: Filesystem access for Nokia phones
Version: 1.0
Release: 1
License: GPL
Group: Hardware/Mobile
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/build-root-%{name}
Packager: Ed Rosten <er258@cam.ac.uk>
Distribution: SuSE
Prefix: /usr
Url: http://mi.eng.cam.ac.uk/~er258/code/

%description 
Gnokiifs is an application which allows Nokia mobile phones to be mounted
as an ordinary filesystem.

%prep
rm -rf $RPM_BUILD_ROOT 
mkdir $RPM_BUILD_ROOT

%setup -q 

%build
%configure
make

%install
/bin/rm -rf "$RPM_BUILD_ROOT"
mkdir -p "$RPM_BUILD_ROOT"/usr/bin
make install DESTDIR=${RPM_BUILD_ROOT}

%post
mkdir /mount/phone
echo "/dev/cfs0	/mnt/phone	coda	defaults,user	0 0" >> /etc/fstab
echo ":0 0600 /dev/cfs0 #GNOKIIFS gnokiifs.rpm" >> /etc/logindevperm

%clean

%files
%attr(0755,root,root) /usr/bin/gnokiifs

